#!/usr/bin/env python3
import sys
from yggdrasil import consts
from yggdrasil.erebor   import runner
from yggdrasil.task_lib import Group
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import Broadcaster
from yggdrasil.task_lib import SerialBroadcaster
from yggdrasil.task_lib import ParrallelBroadcaster
from yggdrasil.task_lib import Ventilator
from yggdrasil.task_lib import TaskProcessor
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent

class Sample(TaskProcessor):
    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        g1 = Group("g1", "g1", "nonexistingcomputernowhere", "0", "root")
        g2 = Group("g2", "g2", "A,A,A,A", "0", "root")

        g2.set_dependency_for_transition(
                consts.IDLE_INIT,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(g1, consts.INIT_RUNNING)]
                    )
                )
        g2.set_trigger_for_transition(
                consts.IDDLE_CANCEL,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(g1, consts.INIT_ERROR)]
                    )
                )
        self.tasks.append(g1)
        self.tasks.append(g2)

if __name__ == "__main__":
    sys.exit(runner(sys.argv, Sample))
