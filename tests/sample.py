#!/usr/bin/env python3
import getopt
import time
import json
import sys
import signal
import threading
from yggdrasil.erebor   import runner
from yggdrasil.erebor   import FrameworkControler
from yggdrasil.erebor   import Erebor
from yggdrasil import consts

def flushprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    sys.stdout.flush()

class Sample(FrameworkControler):
    def __init__(self, erebor, ID, node_list, tfile):
        FrameworkControler.__init__(self, erebor, ID, tfile)

    # notify when some job is done
    def exec_callback(self, data) :
        flushprint("exec brought back {}".format(data))

    # Run some tests
    def start(self, networkId):
        network = self.erebor.networks.get(networkId)

        # Will be called when the network is bootstraped
        def network_root_up(error_nodes) :
            flushprint("\n 'network root has boot, error nodes {}' \n"
                       .format(error_nodes))

            def network_root_update(data):
                flushprint(" network update root {}".format(data))
                def network_root_numbered(data):
                    flushprint(" network renumber root {}".format(data))
                    # now that the network is bootstrap make the node number 4
                    # on the root network execute a command, and gather the
                    # result
                    self.exec_on("4", "uptime", consts.TRUE, "0", "root", "0",
                            "root", self.exec_callback)

                    # Will be called after erebor replication on node 4 on group
                    # root
                    def after_chilren_startup(sub_eredor) :
                        # say that we are well bootstraped
                        print("\n Erebor started on {} \n".format("4"))
                        # send a message to the erebor instance on 4, will only
                        # be visible when logs are enabled
                        network.connector.send_message_to("4", "all", "coucou", False)

                        # Will be called after the group/network nouveau is
                        # started
                        def after_nouveau_startup (networkId) :
                            # notify on screen
                            print("\n Nouveau network started \n")
                            def e_f_up(data):
                                print("\n nouveau -> E and F up")
                                def e_f_numbered(data):
                                    print("\n nouveau -> E and F numbered")
                                    # is called when the taktuk instance in
                                    # control of the nouveau group is ready to
                                    # work
                                    print("\n Le réseau Nouveau est "+
                                            "prêt à travailler \n")
                                    # make some execution without result
                                    # gathering
                                    self.exec_on("0", "uptime", consts.TRUE, "0", "nouveau", "0", "root")
                                    self.exec_on("1", "uptime", consts.TRUE, "0", "nouveau", "0", "root")
                                    self.exec_on("2", "uptime", consts.TRUE, "0", "nouveau", "0", "root")
                                    # make some execution with result gathering
                                    self.exec_on("2", "uptime", consts.TRUE, "0", "nouveau", "0", "root", self.exec_callback)
                                    self.exec_on("1", "uptime", consts.TRUE, "0", "nouveau", "0", "root", self.exec_callback)

                                    # Will be called after group bottom-left has
                                    # bootstrap
                                    def after_bottom_left (networkId) :
                                        flushprint("\n network {} has bootstrap"
                                                  .format(networkId))
                                        # ping pond root bottom-left, ask the
                                        # erebor instance in control of bottom
                                        # left to send a trans network message
                                        # to the root instance, only visible on
                                        # debug
                                        for_root_message = json.dumps({
                                            consts.DEST:"0",
                                            consts.TARGET:"all",
                                            consts.ACTION:consts.MESSAGE,
                                            consts.SYNCHRO:consts.TRUE,
                                            consts.DATA:"coucou from bleft"})
                                        self.tnet_msg_on(for_root_message,
                                                "0", "root", "0", "bottom-left",
                                                "0", "bottom-left","0","root")
                                        # Will be called after the group last is
                                        # bootstrap
                                        def after_last (networkId) :
                                            flushprint("\n network {} has "+
                                                       "bootstrap"
                                                       .format(networkId))
                                            # ping pond root, same than
                                            # bottom-left
                                            for_root_message = json.dumps({
                                                consts.DEST:"0",
                                                consts.TARGET:"all",
                                                consts.ACTION:consts.MESSAGE,
                                                consts.SYNCHRO:consts.TRUE,
                                                consts.DATA:"coucou from last"})
                                            self.tnet_msg_on(for_root_message,
                                                    "0", "root", "0", "last",
                                                    "0", "last","0","root")
                                            # make bottom-left ping bottom right
                                            for_root_message = json.dumps({
                                                consts.DEST:"0",
                                                consts.TARGET:"all",
                                                consts.ACTION:consts.MESSAGE,
                                                consts.SYNCHRO:consts.TRUE,
                                                consts.DATA:"coucou frm bleft"})
                                            self.tnet_msg_on(for_root_message,
                                                    "0", "bottom-right", "0",
                                                    "bottom-left", "0",
                                                    "bottom-left","0","root")

                                            # Is called after "last" group
                                            # shutdown
                                            def after_last_shutdown(net_ID) :
                                                flushprint("\nlast is shutdown")

                                                # make node 4 from root drop the
                                                # network nouveau
                                                def nouveau_shutdown(i):
                                                    flushprint(
                                                    "\n nouveau is shutdown")
                                                    self.erebor.terminate()
                                                def bottom_left_shutdown(i):
                                                    flushprint(
                                                    "\n b_left is shutdown")
                                                def bottom_right_shutdown(i):
                                                    flushprint(
                                                    "\n b_right is shutdown")
                                                # register terminaison on all
                                                # remaining networks
                                                self.erebor.on_network_shutdown(
                                                        "nouveau",
                                                        nouveau_shutdown)
                                                self.erebor.on_network_shutdown(
                                                        "bottom-left",
                                                        bottom_left_shutdown)
                                                self.erebor.on_network_shutdown(
                                                        "bottom-right",
                                                        bottom_right_shutdown)

                                                # demand terminaison of nouveau
                                                # network
                                                self.delete_network("nouveau",
                                                        "0", "nouveau", "0",
                                                        "root")
                                            # Ask to shutdown the network last
                                            self.erebor.on_network_shutdown(
                                                    "last",
                                                    after_last_shutdown)
                                            # terminate the child 1
                                            self.terminate("1", "0", "root",
                                                    "0", "root")
                                        # register callback on group "last"
                                        # initialisation
                                        self.erebor.on_network_init(
                                                "last", after_last)
                                        # replicate on node 1 on group root and
                                        # demand a creation of a network called
                                        # last
                                        self.replic_on(
                                                "1", "last", "", "0", "root",
                                                "0", "root")
                                    # register callback on group "bottom-left"
                                    # initialisation
                                    self.erebor.on_network_init(
                                            "bottom-left", after_bottom_left)
                                    # Replicate on node 1 of network nouveau and
                                    # demand the creation of a network
                                    # bottom-left
                                    self.replic_on("1", "bottom-left", "", "0",
                                            "nouveau", "0", "root")

                                    # is called after the network bottom-right
                                    # initialisation
                                    def after_bottom_right (networkId) :
                                        flushprint("\n network {} has"+
                                                "bootstrap".format(networkId))
                                        # ping pond root bottom-right, like above
                                        for_root_message = json.dumps({
                                            consts.DEST:"0",
                                            consts.TARGET:"all",
                                            consts.ACTION:consts.MESSAGE,
                                            consts.SYNCHRO:consts.TRUE,
                                            consts.DATA:"coucou from bright"})
                                        self.tnet_msg_on(for_root_message,
                                                "0", "root", "0",
                                                "bottom-right", "0",
                                                "bottom-right","0","root")
                                    # register on bottom-right spawn
                                    self.erebor.on_network_init(
                                            "bottom-right", after_bottom_right)
                                    # replicate on node 2 of network nouveau and
                                    # demand the
                                    # creation of the network bottom-right
                                    self.replic_on("2", "bottom-right", "", "0",
                                            "nouveau", "0", "root")
                                self.renumber_on(consts.TRUE, "0", "nouveau",
                                        "0", "root", e_f_numbered)
                            self.spawn_on("0", "E,F", consts.TRUE,
                                          "0", "nouveau", "0", "root",
                                          e_f_up)
                        # register callback on network nouveau
                        self.erebor.on_network_init("nouveau",
                                after_nouveau_startup)
                        # make nouveau appear, and give it nodes to bootstrap
                        self.erebor.start_taktuk_on(sub_eredor, "nouveau", "")

                    # register on replication of 4
                    self.erebor.on_children_startup(
                            "4", network, after_chilren_startup)
                    # replicate on children 4 of network root
                    flushprint("\n 'replic on 4' \n")
                    self.erebor.launch_erebor_on("4", network)
                self.renumber_on(consts.TRUE, "0", "root", "0", "root",
                        network_root_numbered)
            self.network_update_on(consts.TRUE, "0", "root", "0", "root",
                    network_root_update)
        self.spawn_on("0", "A,B,C,D,K", consts.TRUE,
                      "0", "root", "0", "root",
                      network_root_up)

if __name__ == "__main__":
    sys.exit(runner(sys.argv, Sample))
