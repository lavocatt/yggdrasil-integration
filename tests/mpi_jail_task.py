#!/usr/bin/env python3
import sys
import os
from yggdrasil import consts
from yggdrasil.erebor   import runner
from yggdrasil.task_lib import MPIJail
from yggdrasil.task_lib import MPIExecutor
from yggdrasil.task_lib import TaskProcessor
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent

class Sample(TaskProcessor):
    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        nb_clients = 10
        g1 = MPIJail("g1", 'server', '', '0', 'root')
        server_jail = MPIExecutor('server_jail', 
                os.environ["PWD"]+"/c_wrapper/server -C {}".format(nb_clients),
                g1, True)
        server_jail.set_dependency_for_transition(
                consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(g1, consts.INIT_RUNNING)]
                    )
                )
        self.tasks.append(g1)
        self.tasks.append(server_jail)
        for i in range(0, nb_clients) :
            gc = MPIJail("jail_client{}".format(i), "client{}".format(i), '', '0', 'root')
            client_jail = MPIExecutor("mpi_executor{}".format(i),
                            os.environ["PWD"]+"/c_wrapper/client -R 1 -S {}".format("server"),
                            gc,True)
            gc.set_dependency_for_transition(
                    consts.IDLE_INIT,
                    Dependency(
                        Dependency.AND,
                        [DependencyEvent(server_jail, consts.INIT_RUNNING)]
                        )
                    )
            client_jail.set_dependency_for_transition(
                    consts.IDLE_INIT,
                    Dependency(
                        Dependency.AND,
                        [DependencyEvent(gc, consts.INIT_RUNNING)]
                        )
                    )
            self.tasks.append(gc)
            self.tasks.append(client_jail)

if __name__ == "__main__":
    sys.exit(runner(sys.argv, Sample))
