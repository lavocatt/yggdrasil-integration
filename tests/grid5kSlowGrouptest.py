#!/usr/bin/env python3
import sys
from yggdrasil.erebor   import runner
from yggdrasil.erebor   import FrameworkControler
from yggdrasil import consts

class Grid5000SlowGroupTest(FrameworkControler):

    def __init__(self, erebor, ID, node_list, tfile):
        FrameworkControler.__init__(self, erebor, ID, tfile)
        self.node_list = node_list
        self.groups    = dict()

    def dead_nodes(self, rank, ID, nodes) :
        assert False, "dead nodes [ from rank {} ID {}] {}".format(rank, ID, nodes)

    def ping_receive(self, txt) :
        print(txt)

    def network_up(self, ID) :
        self.to_wait_groups -= 1
        self.to_wait_pings   = len(self.groups) * len(self.groups)
        print("network UP {} rest to wait {}".format(ID,
            self.to_wait_groups))
        self.log_time("Replication Made")
        if self.to_wait_groups == 0 :
            self.log_time("Replication Finished")
            self.erebor.terminate()
            self.close()

    def start(self, networkId):
        print("\n 'connected' \n")
        network = self.erebor.networks.get(networkId)
        # get notified on dead nodes
        self.erebor.on_dead_nodes(self.dead_nodes)
        # after taktuk bootstraped nodes :
        def network_root_up(error_nodes):
            print("taktuk network connected")
            # after network fully updated
            def network_root_update(data):
                print("taktuk network updated")
                self.log_time("Group ready")
                # spawn a network on each nodes
                network = self.erebor.networks.get(networkId)
                network.register_on_bridge_generic_messages(self.ping_receive)
                spawned = network.connector.spawned_list()
                i       = 0
                self.to_wait_groups = len(spawned) -1
                for spawn in spawned :
                    if not spawn.me :
                        print(" start on node {} with rank{}".format(spawn.name, spawn.rank))
                        i += 1
                        group = "group{}".format(i)
                        self.groups[group] = spawn
                        self.erebor.on_network_init(group, self.network_up)
                        self.replic_on(str(spawn.rank), group, "", "0", "root",
                                                    "0", "root")
            self.network_update_on(consts.TRUE, "0", "root", "0", "root",
                    network_root_update)
        self.spawn_on("0",self.node_list, consts.FALSE, "0", "root", "0", "root",
                      network_root_up)

if __name__ == "__main__":
    sys.exit(runner(sys.argv, Grid5000SlowGroupTest))
