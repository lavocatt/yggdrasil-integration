#!/usr/bin/env python3
import traceback
import cProfile
import getopt
import time
import json
import sys
import signal
import threading
from yggdrasil.erebor   import runner
from yggdrasil.erebor   import FrameworkControler
from yggdrasil.erebor   import Erebor
from yggdrasil import consts

def flushprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    sys.stdout.flush()

class Grid5000GroupTest(FrameworkControler):

    def __init__(self, erebor, ID, node_list, tfile):
        FrameworkControler.__init__(self, erebor, ID, tfile)
        self.node_list = node_list
        self.groups    = dict()

    def dead_nodes(self, rank, ID, nodes) :
        assert False, "dead nodes [ from rank {} ID {}] {}".format(rank, ID, nodes)

    def ping_receive(self, txt) :
        self.to_wait_pings -= 1
        print(txt)
        if self.to_wait_pings == 0 :
            self.log_time("all pings received")
            self.erebor.terminate()
            self.close()

    def network_up(self, ID) :
        self.to_wait_groups -= 1
        self.to_wait_pings   = len(self.groups) * len(self.groups)
        flushprint("network UP {} rest to wait {}".format(ID,
            self.to_wait_groups))
        if self.to_wait_groups == 0 :
            self.log_time("replication done")
            for pivot_group, spawn in self.groups.items() :
                if not spawn.me :
                    for transit_group, spawn in self.groups.items() :
                        if not spawn.me :
                            print("make {} ping root by {}".format(pivot_group,
                                transit_group))
                            self.make_me_ping_by(pivot_group, transit_group)

    def make_me_ping_by(self, pivot_dnetid, transit_dnetid):
        for_root_message = json.dumps({
            consts.DEST:"0",
            consts.TARGET:"all",
            consts.ACTION:consts.MESSAGE,
            consts.SYNCHRO:consts.TRUE,
            consts.DATA:"coucou from {}".format(transit_dnetid)})
        order = json.dumps({
            consts.DNODE:"0",
            consts.DNETID:"root",
            consts.SNODE:"0",
            consts.SNETID:transit_dnetid,
            consts.ACTION:consts.TNETWRK,
            consts.DATA:for_root_message})
        self.tnet_msg_on(order, "0", transit_dnetid,
                                "0", pivot_dnetid,
                                "0", pivot_dnetid,
                                "0","root")

    def start(self, networkId):
        flushprint("\n 'connected' \n")
        network = self.erebor.networks.get(networkId)
        self.log_time("connect")
        # get notified on dead nodes
        self.erebor.on_dead_nodes(self.dead_nodes)
        # after taktuk bootstraped nodes :
        def network_root_up(error_nodes):
            flushprint("network connected")
            self.log_time("network up")
            # after network fully updated
            def network_root_update(data):
                flushprint("network updated")
                self.log_time("network updated")
                # spawn a network on each nodes
                network = self.erebor.networks.get(networkId)
                network.register_on_bridge_generic_messages(self.ping_receive)
                spawned = network.connector.spawned_list()
                i       = 0
                self.to_wait_groups = len(spawned) -1
                for spawn in spawned :
                    if not spawn.me :
                        flushprint(" start on node {} with rank{}".format(spawn.name, spawn.rank))
                        i += 1
                        group = "group{}".format(i)
                        self.groups[group] = spawn
                        self.erebor.on_network_init(group, self.network_up)
                        self.replic_on(str(spawn.rank), group, "", "0", "root",
                                                    "0", "root")
            self.network_update_on(consts.TRUE, "0", "root", "0", "root",
                    network_root_update)
        self.spawn_on("0",self.node_list, consts.FALSE, "0", "root", "0", "root",
                      network_root_up)

if __name__ == "__main__":
    sys.exit(runner(sys.argv, Grid5000GroupTest))
