#!/usr/bin/env python3
import sys
from yggdrasil import consts
from yggdrasil.erebor   import runner
from yggdrasil.task_lib import Group
from yggdrasil.task_lib import NumberedGroup
from yggdrasil.task_lib import Broadcaster
from yggdrasil.task_lib import SerialBroadcaster
from yggdrasil.task_lib import ParrallelBroadcaster
from yggdrasil.task_lib import Ventilator
from yggdrasil.task_lib import TaskProcessor
from yggdrasil.task_lib import Dependency
from yggdrasil.task_lib import DependencyEvent

class Sample(TaskProcessor):
    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        g1 = Group("g1", "g1", "A,B", "0", "root")
        b1 = Broadcaster("b1", "uptime", g1)
        b1.set_dependency_for_transition(
                consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(g1, consts.INIT_RUNNING)]
                    )
                )
        self.tasks.append(g1)
        self.tasks.append(b1)

        ### serial broadcast after b1 on g1
        b2 = SerialBroadcaster("b2", ["pouet", "pwd"], g1)
        b2.set_dependency_for_transition(
                consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(b1, consts.INIT_RUNNING)]
                    )
                )
        self.tasks.append(b2)

        # parallel broadcast after b2 on g1
        b3 = ParrallelBroadcaster("b3", ["df", "uname -a", "date", "date"], g1, True)
        b3.set_dependency_for_transition(
                consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(b2, consts.RUNNING_ERROR)]
                    )
                )
        self.tasks.append(b3)

        g2 = NumberedGroup("g2", "g2", "A,A,A,A", "0", "root")
        g2.set_dependency_for_transition(
                consts.IDLE_INIT,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(g1, consts.RUNNING_DONE)]
                    )
                )
        self.tasks.append(g2)

        ## ventilator while b3 on g2
        v1 = Ventilator("v1", ["uptime", "uptime", "uptime", "uptime", "uptime",
                        "uptime", "uptime", "uptime", "nono"], g2,True, -1, 1)
        v1.set_dependency_for_transition(
                consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(g2, consts.INIT_RUNNING)]
                    )
                )
        self.tasks.append(v1)

        ## On a new group g3
        g3 = NumberedGroup("g3", "g3", "A", "0", "root")
        g3.set_dependency_for_transition(
                consts.IDLE_INIT,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(g1, consts.RUNNING_DONE)]
                    )
                )
        self.tasks.append(g3)

        ## broadcast that can timeout after v1 done
        b4 = Broadcaster("b4", "sleep 2", g3, False, 1)
        b4.set_dependency_for_transition(
                consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    [DependencyEvent(v1, consts.RUNNING_DONE)]
                    )
                )
        self.tasks.append(b4)

        ## ventilator executed if b4 timeout, canceled otherwise
        v2 = Ventilator("v2", ["echo timeout"], g3, True)
        v2.set_dependency_for_transition(
                consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    [
                        DependencyEvent(b4, consts.RUNNING_TIMEOUT),
                        ]
                    )
                )
        v2.set_trigger_for_transition(
                consts.INIT_CANCELED,
                Dependency(
                    Dependency.AND,
                    [
                        DependencyEvent(b4, consts.RUNNING_DONE),
                        ]
                    )
                )
        self.tasks.append(v2)
        v3 = Ventilator("v3", ["echo pas de timeout"], g3, True)
        v3.set_dependency_for_transition(
                consts.INIT_RUNNING,
                Dependency(
                    Dependency.AND,
                    [
                        DependencyEvent(b4, consts.RUNNING_DONE),
                        ]
                    )
                )
        v3.set_trigger_for_transition(
                consts.INIT_CANCELED,
                Dependency(
                    Dependency.AND,
                    [
                        DependencyEvent(b4, consts.RUNNING_TIMEOUT),
                        ]
                    )
                )
        self.tasks.append(v3)
        g3.set_dependency_for_transition(
                consts.RUNNING_DONE,
                Dependency(
                    Dependency.OR,
                    [
                        Dependency(
                            Dependency.AND,
                            [
                                DependencyEvent(v2, consts.INIT_CANCELED),
                                DependencyEvent(v3, consts.RUNNING_DONE)
                            ]
                        ),
                        Dependency(
                            Dependency.AND,
                            [
                                DependencyEvent(v2, consts.RUNNING_DONE),
                                DependencyEvent(v3, consts.INIT_CANCELED)
                            ]
                        )
                    ]
                )
            )


        ## append all tasks to the list
        #self.tasks.append(v3)

if __name__ == "__main__":
    sys.exit(runner(sys.argv, Sample))
